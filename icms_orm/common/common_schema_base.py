from typing import Type
from icms_orm.orm_interface.model_base_class import IcmsModelBase
from icms_orm import cms_common_bind_key, cms_common_schema_name, icms_reader_role
from icms_orm import IcmsDeclarativeBasesFactory
from sqlalchemy import event
from sqlalchemy.sql.ddl import DDL
import logging


DeclarativeBase: Type[IcmsModelBase] = IcmsDeclarativeBasesFactory.get_for_bind_key(cms_common_bind_key())


def _subclass_visitor(cls):
    logging.debug('COMMON SCHEMA TABLES VISITOR INVOKED!')
    if hasattr(cls, '__table__'):
        # todo: should be table-specific, but well...
        ddl = 'GRANT SELECT, INSERT, UPDATE, REFERENCES ON ALL TABLES IN SCHEMA public to {role}'.format(role=icms_reader_role())
        event.listen(cls.__table__, 'after_create', DDL(ddl))


class CommonBaseMixin():
    __bind_key__ = cms_common_bind_key()
    __table_args__ = {'schema': cms_common_schema_name()}   
    __icms_subclass_visit_function__ = _subclass_visitor