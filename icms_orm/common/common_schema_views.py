from icms_orm import DatabaseViewMixin, cms_common_role_name
from icms_orm.common.common_schema_base import CommonBaseMixin, DeclarativeBase
from sqlalchemy.types import Integer, String, Boolean
from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import JSONB
from icms_orm.common.common_schema_people_tables import person_status_enum


def _class_visit_fn(cls):
    CommonBaseMixin.__icms_subclass_visit_function__(cls)
    DatabaseViewMixin.__icms_subclass_visit_function__(cls)


class CommonSchemaViewBase(CommonBaseMixin, DatabaseViewMixin):
    __bind_key__ = CommonBaseMixin.__bind_key__
    __table_args__ = CommonBaseMixin.__table_args__
    __icms_subclass_visit_function__ = _class_visit_fn

    @classmethod
    def get_schema_name(cls) -> str:
        return cls.__table_args__.get('schema') or ''

    @classmethod
    def get_role(cls, inner_sql='') -> str:
        return cms_common_role_name() or ''


class PersonFlagsView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person_flags'
    __icms_dependencies__ = ['public.legacy_flag']

    cms_id = Column(Integer, primary_key=True)
    flags = Column(JSONB)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return 'select cms_id, json_agg(flag_code) as flags from legacy_flag ' + \
            'where now() between start_date and coalesce(end_date, now()) group by cms_id'


class PersonInstCodeView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person_inst_code'
    __icms_dependencies__ = ['public.affiliation']
    cms_id = Column(Integer, primary_key=True)
    inst_code = Column(String(32))

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return 'select cms_id, inst_code from affiliation where is_primary=true ' + \
               'and now() between start_date and coalesce(end_date, now())'


class PersonAffiliationsView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person_affiliations'
    __icms_dependencies__ = ['public.affiliation']
    cms_id = Column(Integer, primary_key=True)
    affiliations = Column(JSONB)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return "select cms_id, json_object_agg(inst_code, case when is_primary then 'primary' else 'secondary' end) " + \
            'as affiliations from affiliation ' + \
            'where now() between start_date and coalesce(end_date, now()) group by cms_id'


class PersonProjectsView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person_projects'
    __icms_dependencies__ = ['public.assignment']
    cms_id = Column(Integer, primary_key=True)
    affiliations = Column(JSONB)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return 'select cms_id, json_object_agg(project_code, fraction) ' + \
            'as projects from assignment ' + \
            'where now() between start_date and coalesce(end_date, now()) group by cms_id'


class PersonTeamDutiesView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person_team_duties'
    __icms_dependencies__ = ['public.institute_leader']
    cms_id = Column(Integer, primary_key=True)
    team_duties = Column(JSONB)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return "select cms_id, json_object_agg(inst_code, case when is_primary=true then 'leader' else 'deputy' end) as team_duties from institute_leader " + \
            "where now() between start_date and coalesce(end_date, now()) " + \
            "group by cms_id"


class PersonManagedUnitIdsView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person_managed_unit_ids'
    __icms_dependencies__ = ['public.tenure', 'public.org_unit', 'public.position']
    cms_id = Column(Integer, primary_key=True)
    managed_unit_ids = Column(JSONB)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return "select cms_id, json_agg(ou.id) as managed_unit_ids from tenure t " + \
            "join org_unit ou on (t.unit_id = ou.id or t.unit_id = ou.superior_unit_id) " + \
            "join position p on (p.id = t.position_id and p.level <= 1) " + \
            "where now() between t.start_date and coalesce(t.end_date, now()) " + \
            "group by cms_id"


class PersonStatusView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person_status'
    __icms_dependencies__ = ['public.person_status']

    cms_id = Column(Integer, primary_key=True)
    status = Column(person_status_enum)
    activity = Column(String)
    author_block = Column(Boolean)
    is_author = Column(Boolean)
    epr_suspension = Column(Boolean)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return "select cms_id, status, activity, author_block, is_author, epr_suspension from person_status ps " + \
            "where now() between ps.start_date and coalesce(ps.end_date, now())"


class PersonView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person'
    __icms_dependencies__ = ['public.{0}'.format(_t) for _t in [
        'person', 'view_person_status', 'view_person_affiliations', 'view_person_projects', 'view_person_team_duties', 'view_person_flags', 'view_person_managed_unit_ids'
    ]]
    cms_id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    hr_id = Column(String)
    login = Column(String)
    status = Column(person_status_enum)
    activity = Column(String)
    is_author = Column(Boolean)
    author_block = Column(Boolean)
    epr_suspension = Column(Boolean)
    affiliations = Column(JSONB)
    projects = Column(JSONB)
    team_duties = Column(JSONB)
    flags = Column(JSONB)
    managed_unit_ids = Column(JSONB)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return "select cms_id, first_name, last_name, hr_id, login, status, activity, is_author, author_block, epr_suspension, affiliations, projects, team_duties, flags, managed_unit_ids from person " + \
            "left outer join view_person_status using (cms_id) " + \
            "left outer join view_person_affiliations using (cms_id) " + \
            "left outer join view_person_projects using (cms_id) " + \
            "left outer join view_person_team_duties using (cms_id) " + \
            "left outer join view_person_flags using (cms_id) " + \
            "left outer join view_person_managed_unit_ids using (cms_id)"
