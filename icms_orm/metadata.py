#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from typing import Type
from icms_orm.orm_interface.model_base_class import IcmsModelBase
from sqlalchemy.types import String, Integer, Enum, DateTime
from icms_orm import PseudoEnum
from sqlalchemy import Column

from icms_orm import metadata_bind_key, metadata_schema_name, IcmsDeclarativeBasesFactory

DeclarativeBase: Type[IcmsModelBase] = IcmsDeclarativeBasesFactory.get_for_bind_key(metadata_bind_key())


class MetadataBaseMixin():
    __bind_key__ = metadata_bind_key()
    __table_args__ = {'schema': metadata_schema_name()}


class OperationValues(PseudoEnum):
    INSERT = 'insert'
    UPDATE = 'update'
    DELETE = 'delete'


class ChangeLog(MetadataBaseMixin, DeclarativeBase):
    __tablename__ = 'ChangeLog'

    id = Column('id', Integer, primary_key=True, autoincrement=True)
    ref_schema = Column(String(64), nullable=False)
    ref_table = Column(String(64), nullable=False)
    ref_id = Column(Integer)
    ref_hash_id = Column(String(32))
    # Native enum disabled as SQLAlchemy kept trying to create it on the postgres side during testing.
    # Looks like a fine workaround as this table will not be managed by alembic or SQLAlchemy except when testing.
    operation = Column(Enum(*OperationValues.values(), schema=metadata_schema_name(), name='row_operation_name', metadata=DeclarativeBase.metadata))
    last_update = Column(DateTime, nullable=False)
