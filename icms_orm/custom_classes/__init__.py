from typing import Optional
from icms_orm.custom_classes.db_view_mixin import DatabaseViewMixin
import os


class OverridableConfigOption(object):
    """
    Allows overriding certain configuration options before they get propagated into dependant classes.
    Should an attempt be made to `.override()` a property that has already been read - an exception is thrown.
    """

    def __init__(self, initial_value: Optional[str], env_override_key: Optional[str] = None):
        """
        Should a corresponding environment variable be found, its value will take precedence
        and overwrite the initial_value during the first access attempt - as in __call__
        """
        self._value = initial_value
        self._env_override_key = env_override_key
        self._accessed = False

    def __call__(self) -> Optional[str]:
        if not self._accessed and self._env_override_key is not None and self._env_override_key in os.environ.keys():
            self._value = os.environ.get(self._env_override_key)
        self._accessed = True
        if callable(self._value):
            # one option can refer to another for its value (or to any callable really)
            self._value = self._value()
        return self._value

    def override(self, new_value: str):
        if self._value == new_value:
            return
        assert self._accessed is False, 'OverridableConfigOption has already been read and so it cannot be overridden. Intended override: {0}'.format(new_value)
        self._value = new_value
        self._env_override_key = None


class PseudoEnum(object):
    @classmethod
    def values(cls):
        return [value for key, value in vars(cls).items() if not (key.startswith('__') or callable(value) or type(value) in (classmethod, staticmethod))]


__all__ = ['DatabaseViewMixin', 'OverridableConfigOption', 'PseudoEnum']
