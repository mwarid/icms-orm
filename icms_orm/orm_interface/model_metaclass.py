import enum
from typing import Type
from icms_orm.orm_interface.model_base_class import IcmsModelBase
import logging
from sqlalchemy.ext.declarative.api import DeclarativeMeta

log: logging.Logger = logging.getLogger(__name__)


class IcmsDeclarativeModelMetaclass(DeclarativeMeta):
    """
    Post-Alchy metaclass. Performs some of the incantations its predecessor would
    as well as the steps necessary to initialize the DB views and generally fill in
    the blanks provided by IcmsModelBase.
    """

    class MetaPropName(enum.Enum):
        # should be a callable
        SUBCLASS_VISIT_FUNCTION = '__icms_subclass_visit_function__'
        # an array of strings representing other DB objecs' names
        DEPENDENCIES = '__icms_dependencies__'
        # Alchy had it...
        EVENTS = '__events__'
        # flask-sqlalchemy and alchy had it...
        BIND_KEY = '__bind_key__'

    def __init__(cls: Type[IcmsModelBase], name, bases, dct):
        super().__init__(name, bases, dct)
        visit_function = dct.get(
            IcmsDeclarativeModelMetaclass.MetaPropName.SUBCLASS_VISIT_FUNCTION.value)
        if callable(visit_function):
            visit_function(cls)

        dependencies = dct.get(
            IcmsDeclarativeModelMetaclass.MetaPropName.DEPENDENCIES.value)
        if dependencies:
            for _dep in dependencies:
                metadata = dct.get('metadata') or cls.__table__.metadata
                cls.__table__.add_is_dependent_on(metadata.tables[_dep])
        if IcmsDeclarativeModelMetaclass.MetaPropName.BIND_KEY.value in dct:
            cls.__table__.info['bind_key'] = dct[IcmsDeclarativeModelMetaclass.MetaPropName.BIND_KEY.value]

    def __new__(cls, name, bases, dct):
        dct[IcmsDeclarativeModelMetaclass.MetaPropName.EVENTS.value] = dct.get(
            IcmsDeclarativeModelMetaclass.MetaPropName.EVENTS.value, {})
        _found_bases = []
        _bases_to_check = bases
        while len(_bases_to_check) > 0:
            _deeper_bases = []
            for _base in _bases_to_check:
                _found_bases.append(_base)
                for _base_of_base in _base.__bases__:
                    _deeper_bases.append(_base_of_base)
            _bases_to_check = _deeper_bases

        base_dcts = [dct] + [base.__dict__ for base in reversed(_found_bases)]

        # Propagating the visitor function so that it can be called when the cls.__table__ is available.
        for base_dct in base_dcts:
            for _class_key in [e.value for e in IcmsDeclarativeModelMetaclass.MetaPropName]:
                if _class_key in base_dct.keys():
                    log.debug('Found and propagating meta property {0} for class {1}'.format(
                        _class_key, name))
                    dct[_class_key] = base_dct[_class_key]

        cls = super().__new__(cls, name, bases, dct)
        return cls
