from functools import partial
from typing import Any, Dict, List, Optional, Type
from sqlalchemy.engine.base import Engine
from sqlalchemy.orm.mapper import Mapper

from sqlalchemy.orm.query import Query
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.sql.schema import Table
from sqlalchemy.sql import ClauseElement
from icms_orm.orm_interface.model_base_class import IcmsModelBase
from sqlalchemy.orm.session import Session
from sqlalchemy import MetaData
from sqlalchemy import create_engine
from sqlalchemy.engine.url import make_url
from icms_orm.orm_interface.declarative_bases_factory import \
    IcmsDeclarativeBasesFactory
from icms_orm.orm_interface.orm_utils import BindKeyExtractor
import sqlalchemy as sa


class IcmsOrmSession(Session):
    """
    Post-Alchy Session class with bind-switching capability.
    """

    def __init__(self, manager: 'OrmManager', **options):
        """
        Grabbed from Alchy.
        """
        self.manager = manager
        bind = options.pop('bind', manager.engine)
        binds = options.pop('binds', {}) or manager.binds_map
        super().__init__(bind=bind, binds=binds, **options)

    def get_bind(self, mapper: Optional[Mapper] = None, clause: Optional[ClauseElement] = None) -> Engine:
        """
        This method still follows the approach impposed by flask-sqlalchemy / alchy.
        We could perhaps make this work based on checking which of the registered
        declarative bases is involved - and then perhaps trim away all the __bind_key__ business.
        (might be tricky for ClauseElement instances)
        """
        bind_key = BindKeyExtractor.safely_extract_from_any(mapper, clause)
        if bind_key:
            return self.manager.get_engine(bind=bind_key)
        return super().get_bind(mapper, clause)


class EngineOption:
    def __init__(self, option_key, option_value, engine_uri=None):
        self.option_key = option_key
        self.option_value = option_value
        self.engine_uri = engine_uri

    def is_applicable_for(self, engine_uri):
        return self.engine_uri is None or self.engine_uri == engine_uri


class IcmsOrmQuery(Query):
    """
    Post-Alchy query class. Takes over only what's needed to be backward-compatible.
    """
    @property
    def entities(self):
        return [e.mapper.class_ for e in self._entities]

    def joinedload(self, relationship_field):
        return self.options(sa.orm.joinedload(relationship_field))


class OrmManager:
    """
        Post-Alchy Manager class. Incorporates some of its once-superclass' methods
    """
    def __init__(self, config: Optional[Dict[str, Any]] = None, session_options=None, session_class=IcmsOrmSession):
        self.config = {
            'SQLALCHEMY_DATABASE_URI': 'sqlite://',
            'SQLALCHEMY_BINDS': None,
            'SQLALCHEMY_ECHO': False,
            'SQLALCHEMY_POOL_SIZE': None,
            'SQLALCHEMY_POOL_TIMEOUT': None,
            'SQLALCHEMY_POOL_RECYCLE': None,
            'SQLALCHEMY_MAX_OVERFLOW': None
        }
        self.config.update(config or dict())
        self._engines = {}
        self._binds = {}
        self._binds.update(self.config['SQLALCHEMY_BINDS'])
        self._binds[None] = self._binds.get(
            None, self.config['SQLALCHEMY_DATABASE_URI'])

        if session_options is None:
            session_options = {}

        session_options.setdefault('query_cls', IcmsOrmQuery)
        session_options.setdefault('autocommit', False)
        session_options.setdefault('autoflush', True)

        self.session_class = session_class or IcmsOrmSession
        self.session = self.create_scoped_session(session_options)
        self._engine_options = list()
        self._prepopulate_engine_options_with_config_values()
        IcmsModelBase.session = self.session

    @property
    def binds(self):
        """
        Grabbed from Alchy.
        """
        return self._binds

    @property
    def binds_map(self) -> Dict[Table, Engine]:
        """
        Grabbed from Alchy. Returns a dictionary with a table->engine mapping.
        """
        binds = list(self.binds)
        retval = {}
        for bind in binds:
            engine = self.get_engine(bind)
            tables = self.get_tables_for_bind_key(bind)
            retval.update(dict((table, engine) for table in tables))
        return retval

    @property
    def engine(self):
        """
        Grabbed from Alchy.
        """
        return self.get_engine()

    def get_engine(self, bind=None) -> Engine:
        """
        Grabbed from Alchy. Returns engine associated with bind (creating one if needed).
        """
        if bind not in self._engines:
            assert bind in self.binds, (
                f'Bind {bind} is not specified. '
                'Set in SQLALCHEMY_BINDS configuration variable')
            self._engines[bind] = self.create_engine(self.binds[bind])
        return self._engines[bind]

    def create_session(self, options) -> IcmsOrmSession:
        return self.session_class(self, **options)

    def create_scoped_session(self, options=None) -> scoped_session:
        """
        Grabbed from Alchy. Creates scoped session which internally calls :meth:`create_session`.
        """
        if options is None:
            options = {}
        return scoped_session(partial(self.create_session, options))

    def _prepopulate_engine_options_with_config_values(self):
        for sa_key, config_key in [('echo', 'SQLALCHEMY_ECHO'),
                                   ('pool_size', 'SQLALCHEMY_POOL_SIZE'),
                                   ('pool_timeout', 'SQLALCHEMY_POOL_TIMEOUT'),
                                   ('pool_recycle', 'SQLALCHEMY_POOL_RECYCLE'),
                                   ('max_overflow', 'SQLALCHEMY_MAX_OVERFLOW')]:
            if self.config.get(config_key, None) is not None:
                self.register_engine_option(EngineOption(
                    sa_key, self.config.get(config_key), None))

    def get_bind_keys(self) -> List[str]:
        return self.config.get('SQLALCHEMY_BINDS').keys()

    def get_sqlalchemy_session_binds_map(self):
        the_map = dict()
        for _bk in self.get_bind_keys():
            _meta = IcmsDeclarativeBasesFactory.get_for_bind_key(_bk)
            _engine = self.get_engine_for_bind_key(_bk)
            the_map[_meta] = _engine
        return the_map

    def create_all(self, bind: str = '__all__'):
        _tables = self.get_tables_for_bind_key(bind)
        self.create_or_drop_all_tables_at_once(_tables, create=True)

    def drop_all(self, bind: str = '__all__'):
        _tables = self.get_tables_for_bind_key(bind)
        self.create_or_drop_all_tables_at_once(_tables, create=False)

    def create_or_drop_all_tables_at_once(self, tables, create=True):
        self.session.rollback()
        if tables:
            _metadata = tables[0].metadata
            _bind_key = tables[0].info.get('bind_key')
            _metadata.bind = self.get_engine_for_bind_key(_bind_key)
            if create:
                MetaData.create_all(
                    self=_metadata,
                    tables=tables,
                    checkfirst=True
                )
            else:
                MetaData.drop_all(
                    self=_metadata,
                    tables=list(reversed(tables)),
                    checkfirst=True
                )
            _metadata.bind = None

    def get_engine_for_bind_key(self, bind_key):
        return self.get_engine(bind=bind_key)

    def get_declarative_base_for_bind_key(self, bind_key: str):
        return IcmsDeclarativeBasesFactory.get_for_bind_key(bind_key)

    def get_metadata_for_bind_key(self, bind_key: str):
        return self.get_declarative_base_for_bind_key(bind_key).metadata

    def get_tables_for_bind_key(self, bind_key: str, metadata: MetaData = None):
        bind_keys = self.binds.keys() if bind_key == '__all__' else [bind_key]
        _all_tables = []
        for _bk in bind_keys:
            metadata = metadata or self.get_metadata_for_bind_key(bind_key=_bk)
            # double checking for the registered bind_key as using single metadata instance across binds would reveal all tables here and now
            tables = [t for t in metadata.sorted_tables if t.info.get(
                'bind_key') == _bk]
            _all_tables += tables
        return _all_tables

    # Overrides superclass' method - thus the funny params' names
    def create_engine(self, uri_or_config: str):
        uri = uri_or_config
        options = self.get_options_for_engine(uri)
        return create_engine(make_url(uri), **options)

    def register_engine_option(self, engine_option: EngineOption):
        self._engine_options.append(engine_option)

    def get_options_for_engine(self, engine_uri=None):
        options = {}
        for engine_option in self._engine_options:
            assert isinstance(engine_option, EngineOption)
            if engine_option.is_applicable_for(engine_uri):
                options[engine_option.option_key] = engine_option.option_value
        return options

    @property
    def query(self) -> Type[Query]:
        return self.session.query
