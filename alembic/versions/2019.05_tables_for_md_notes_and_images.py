"""Tables for md notes and images

Revision ID: 2019.05
Revises: 2019.04
Create Date: 2019-06-06 13:49:19.905572

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '2019.05'
down_revision = '2019.04'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('image_upload',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=64), nullable=False),
    sa.Column('data', sa.LargeBinary(), nullable=True),
    sa.Column('url', sa.Text(), nullable=True),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_image_upload')),
    sa.UniqueConstraint('name', name=op.f('uq_image_upload_name')),
    schema='public'
    )

    op.create_table('markdown_note',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=64), nullable=False),
    sa.Column('body', sa.Text(), nullable=True),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_markdown_note')),
    sa.UniqueConstraint('name', name=op.f('uq_markdown_note_name')),
    schema='public'
    )


def downgrade():
    op.drop_table('markdown_note', schema='public')
    op.drop_table('image_upload', schema='public')

