"""some new columns

Revision ID: 2021.01
Revises: 2020.02
Create Date: 2021-03-05 15:24:01.288342

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '2021.01'
down_revision = '2020.02'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('institute', sa.Column('enum_code', sa.String(length=16), nullable=True))
    op.add_column('institute', sa.Column('fa_id', sa.Integer(), nullable=True))
    op.add_column('person', sa.Column('date_of_birth', sa.Date(), nullable=True))
    op.create_foreign_key(op.f('fk_institute_fa_id_funding_agency'), 'institute', 'funding_agency', ['fa_id'], ['id'], source_schema='public', referent_schema='public', onupdate='CASCADE', ondelete='SET NULL')


def downgrade():
    op.drop_constraint(op.f('fk_institute_fa_id_funding_agency'), 'institute', schema='public', type_='foreignkey')
    op.drop_column('institute', 'fa_id')
    op.drop_column('institute', 'enum_code')
    op.drop_column('person', 'date_of_birth')