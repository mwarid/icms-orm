"""db_changelog

Revision ID: 2020.02
Revises: 2020.01
Create Date: 2020-05-04 12:54:22.330363

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '2020.02'
down_revision = '2020.01'
branch_labels = None
depends_on = None


def upgrade():
    from icms_orm.common import row_opertaion_enum
    row_opertaion_enum.create(op.get_bind())

    op.create_table('row_change',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('schema_name', sa.String(length=32), nullable=False),
        sa.Column('table_name', sa.String(length=64), nullable=False),
        sa.Column('primary_key', sa.Integer(), nullable=True),
        sa.Column('composite_key', postgresql.JSONB(astext_type=sa.Text()), nullable=True),
        sa.Column('former_values', postgresql.JSONB(astext_type=sa.Text()), nullable=True),
        sa.Column('new_values', postgresql.JSONB(astext_type=sa.Text()), nullable=True),
        sa.Column('operation', row_opertaion_enum, nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('cms_id', sa.Integer(), nullable=True),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_row_change')),
        schema='public'
    )


def downgrade():
    from icms_orm.common import row_opertaion_enum
    
    op.drop_table('row_change', schema='public')
    row_opertaion_enum.create(op.get_bind())
