"""drop_tables_for_job_opening_workflow

Revision ID: 2022.02
Revises: 2022.01
Create Date: 2022-12-05 14:21:46.270868

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '2022.02'
down_revision = '2022.01'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_table('job_questionnaire_answers', schema='public')
    op.drop_table('job_nomination_status', schema='public')
    op.drop_table('job_nomination', schema='public')
    op.drop_table('job_opening', schema='public')
    op.drop_table('job_questionnaire', schema='public')


def downgrade():
    op.create_table('job_questionnaire',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('title', sa.String(), nullable=False),
    sa.Column('questions', sa.String(), nullable=False),
    sa.Column('date_created', sa.DateTime(), nullable=True, server_default=sa.func.current_timestamp()),
    sa.Column('date_updated', sa.DateTime(), nullable=True, server_default=sa.func.current_timestamp()),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_job_questionnaire')),
    schema='public'
    )
    op.create_table('job_opening',
    sa.Column('id', sa.SmallInteger(), autoincrement=True, nullable=False),
    sa.Column('title', sa.String(length=200), nullable=False),
    sa.Column('description', sa.String(length=1000), nullable=False),
    sa.Column('requirements', sa.String(length=1000), nullable=False),
    sa.Column('start_date', sa.Date(), nullable=False),
    sa.Column('end_date', sa.Date(), nullable=True),
    sa.Column('deadline', sa.Date(), nullable=False),
    sa.Column('position_id', sa.SmallInteger(), nullable=True),
    sa.Column('status', sa.String(length=80), nullable=False),
    sa.Column('last_modified', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['position_id'], ['public.position.id'], name=op.f('fk_job_opening_position_id_position'), onupdate='CASCADE'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_job_opening')),
    schema='public'
    )
    op.create_table('job_nomination',
    sa.Column('nomination_id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('job_id', sa.Integer(), nullable=False),
    sa.Column('nominee_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['nominee_id'], [u'public.person.cms_id'], name=op.f('fk_job_nomination_nominee_id_person'), onupdate='CASCADE', ondelete='SET NULL'),
    sa.ForeignKeyConstraint(['job_id'], [u'public.job_opening.id'], name=op.f('fk_job_nomination_job_id_job_opening'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('nomination_id', name=op.f('pk_job_nomination')),
    schema='public'
    )
    op.create_table('job_nomination_status',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('actor_id', sa.Integer(), nullable=False),
    sa.Column('nomination_id', sa.Integer(), nullable=False),
    sa.Column('actor_remarks', sa.String(length=1000), nullable=True),
    sa.Column('status', sa.String(length=128), nullable=False),
    sa.Column('last_modified', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['nomination_id'], [u'public.job_nomination.nomination_id'], name=op.f('fk_job_nomination_status_nomination_id_job_nomination'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['actor_id'], [u'public.person.cms_id'], name=op.f('fk_job_nomination_status_actor_id_person'), onupdate='CASCADE', ondelete='SET NULL'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_job_nomination_status')),
    schema='public'
    )
    op.create_table('job_questionnaire_answers',
    sa.Column('id', sa.SmallInteger(), autoincrement=True, nullable=False),
    sa.Column('answers', sa.String(length=10000), nullable=False),
    sa.Column('questionnaire_id', sa.SmallInteger(), nullable=False),
    sa.Column('job_nomination_id', sa.SmallInteger(), nullable=False),
    sa.Column('submitter_id', sa.Integer(), nullable=False),
    sa.Column('submitted_at', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['job_nomination_id'], ['public.job_nomination.nomination_id'], name=op.f('fk_job_questionnaire_answers_job_nomination_id_job_nomination'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['questionnaire_id'], ['public.job_questionnaire.id'], name=op.f('fk_job_questionnaire_answers_questionnaire_id_job_questionnaire'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['submitter_id'], ['public.person.cms_id'], name=op.f('fk_job_questionnaire_answers_submitter_id_person'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_job_questionnaire_answers')),
    schema='public'
    )
