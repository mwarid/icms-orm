import icms_orm
import pytest
from tests.conftest import TestParameters
from icms_orm import OrmManager


@pytest.mark.parametrize('bind_key', TestParameters.bind_keys())
def test_table_definitions_are_discovered(db_manager: OrmManager, test_config, bind_key):
    assert bind_key in test_config.get('SQLALCHEMY_BINDS').keys()
    db_engine = db_manager.get_engine_for_bind_key(bind_key)
    meta_data = icms_orm.IcmsDeclarativeBasesFactory.get_for_bind_key(bind_key).metadata
    meta_data.bind = db_engine
    meta_data.reflect(bind=db_engine, views=True)
    assert len(meta_data.tables) > 0, 'No table definitions found for bind key {0}!'.format(bind_key)


@pytest.mark.parametrize('bind_key', TestParameters.bind_keys())
def test_metadata_does_not_mix_table_binds_up(db_manager, test_config, bind_key):
    """
    This one has aged into a bit of a tautology but it still demonstrates the point at least.
    For every bind key we retrieve the corresponding MetaData instance. 
    Then we ensure that this metadata does not list tables from other binds.
    """

    meta = icms_orm.IcmsDeclarativeBasesFactory.get_for_bind_key(bind_key).metadata

    # The following three lines are useful to check against "ghost" tables 
    db_engine = db_manager.get_engine_for_bind_key(bind_key)
    meta.bind = db_engine
    meta.reflect(db_engine, views=True)
    # These tables appear upon reflection, under None schema / bind, if metadata is misconfigured
    # In particular if the default schema name is not set and thus discovered tables are not matched
    # correctly against those defined in the code.

    for table in meta.sorted_tables:
        assert isinstance(table.info, dict)
        assert table.info.get('bind_key') == bind_key, 'Expected to find {good_bind} as table\'s {table} bind key. Found: {actual_bind}'.\
            format(good_bind=bind_key, table=table.name,
                   actual_bind=table.info.get('bind_key'))
