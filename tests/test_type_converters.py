from icms_orm.orm_interface.orm_manager import OrmManager
from typing import Optional, Type
import pytest
from icms_orm import IcmsDeclarativeBasesFactory
import sqlalchemy as sa
from type_decorators import StringBool, StringInt
from type_decorators import StringDate
from type_decorators import StringDatetime
from type_decorators import StringFloat
from datetime import date, datetime
from icms_orm import cms_people_bind_key


class MockDeclarativeBasesFactory(IcmsDeclarativeBasesFactory):
    pass


@pytest.fixture(scope='module')
def define_mapper(db_manager: OrmManager):
    class TypeFreak(MockDeclarativeBasesFactory.get_for_bind_key(cms_people_bind_key())):
        __bind_key__ = cms_people_bind_key()

        id = sa.Column(sa.Integer, primary_key=True)
        strint = sa.Column(StringInt(length=8))
        strdate = sa.Column(StringDate(format='%m/%y/%d'))
        strbool = sa.Column(StringBool(length=188))
        strdate2 = sa.Column(StringDate(length=32))
        strfloat = sa.Column(StringFloat(length=22))
        strdatetime = sa.Column(StringDatetime(
            format='%Y - %m - %d @ %H : %M : %S'))
        strdatetime2 = sa.Column(StringDatetime(length=128))

        def __init__(self, number: int = 42, the_date: date = date(2019, 11, 27), the_bool: bool = False, the_float: float = 4.2, timestamp: datetime = datetime(1999, 12, 31, 12, 59, 59)):
            self.strint = number
            self.strdate = the_date
            self.strdate2 = the_date
            self.strbool = the_bool
            self.strfloat = the_float
            self.strdatetime = timestamp
            self.strdatetime2 = timestamp

    db_manager.create_all(bind=cms_people_bind_key())
    return TypeFreak


class TestTypeConverters():

    _db_manager: Optional[OrmManager] = None

    @classmethod
    def teardown_class(cls):
        if cls._db_manager is not None:
            cls._db_manager.drop_all(bind=cms_people_bind_key())

    @pytest.mark.parametrize('_int,_bool,_date,_datetime,_float', [
        (168, True, date(2011, 10, 11), datetime(1998, 12, 11, 13, 16), 42.24),
        (11, False, date(1996, 10, 11), datetime(1998, 12, 11, 13, 16), -0.1),
        ('11', False, date(1996, 10, 11), datetime(1998, 12, 11, 13, 16), None),
        (None, None, date(1996, 10, 11), datetime(1998, 12, 11, 13, 16), '167.15')

    ])
    def test_foo(self, define_mapper: Type, db_manager: OrmManager, _int: int, _bool: bool, _date: date, _datetime: datetime, _float: float):
        TypeFreak = define_mapper
        TestTypeConverters._db_manager = db_manager
        ssn = TypeFreak.session
        ssn.add(TypeFreak(_int, _date, _bool, _float, _datetime))
        ssn.commit()
        fetched = ssn.query(TypeFreak).order_by(sa.desc(TypeFreak.id)).first()
        assert int(_int or 0) == fetched.strint
        assert _date == fetched.strdate
        assert _date == fetched.strdate2
        assert bool(_bool or None) == fetched.strbool
        assert float(_float or 0) == fetched.strfloat
        assert _datetime == fetched.strdatetime
        assert _datetime == fetched.strdatetime2
