import pytest
from os import environ
from icms_orm.custom_classes import OverridableConfigOption
import random


class Counter(object):
    def __init__(self, start=0):
        self.count = start
    
    def __call__(self):
        self.count += 1
        return self.count


def test_env_var_takes_precedence():
    environ['ICMS_TESTVAR'] = 'FOOVAR'
    opt = OverridableConfigOption('barefoot', env_override_key='ICMS_TESTVAR')
    assert opt() == 'FOOVAR'


def test_overriding():
    opt = OverridableConfigOption('barefoot')
    opt.override('bigfoot')
    opt.override('yeti')
    assert opt() == 'yeti'


def test_no_overriding_after_reading():
    opt = OverridableConfigOption('barefoot')
    opt()
    _error = None
    try:
        opt.override('yogi!')
    except AssertionError as ae:
        _error = ae
    assert _error is not None


def test_override_erases_env_var():
    environ['ICMS_TESTVAR'] = 'EVERYTHING'
    opt = OverridableConfigOption('nothing', 'ICMS_TESTVAR')
    opt.override(Counter(42))
    assert opt() == 43
    assert opt() == 43


def test_env_var_is_preserved_beyond_first_access():
    environ['ICMS_TESTVAR']='YES'
    opt=OverridableConfigOption('NO', env_override_key='ICMS_TESTVAR')
    opt()
    environ['ICMS_TESTVAR']='MAYBE'
    assert opt() == 'YES'


def test_callables_result_is_preserved_beyond_first_access():
    opt=OverridableConfigOption(Counter(256))
    assert opt() == 257
    assert opt() == 257
    assert opt() == 257


def test_empty_env_var_fallback():
    environ['ICMS_TESTVAR'] = 'ice-scream'
    environ.pop('ICMS_TESTVAR')
    opt = OverridableConfigOption('pudding', env_override_key='ICMS_TESTVAR')
    assert opt() == 'pudding'